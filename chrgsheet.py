import pandas as pd
import numpy as np
data=pd.read_csv("BMJ Ch21.csv",usecols=['Item type','Item description','Measure','Cost ($)'])
data['Item type']=data['Item type'].str.upper()
datadict =data.set_index('Item type').to_dict()['Cost ($)']
print(datadict)

df=pd.read_csv("BMJ Nov21.csv",skipfooter=8,engine='python')
data2 = df.rename(columns=df.iloc[0]).loc[1:]
data2['Item type']=data2['Item type'].str.upper()
print(data2)

data2["Cost$"]=data2['Item type'].map(datadict)
print(data2)